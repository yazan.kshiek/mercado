package com.mercado.product_synchronizer.service;

import com.mercado.product_synchronizer.entity.Product;
import com.mercado.product_synchronizer.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productReposiory;

    public ProductService(ProductRepository productReposiory) {
        this.productReposiory = productReposiory;
    }

    @Transactional
    public Product create(Product product) {
        return productReposiory.save(product);
    }

    @Transactional
    public Product update(Long id, Product product) {
        product.setSku(id);

        Optional<Product> maybeProduct = productReposiory.findById(id);
        if(!maybeProduct.isPresent())
            return productReposiory.save(product);

        Product dbProduct = maybeProduct.get();

        dbProduct.setDescription(product.getDescription());
        dbProduct.setPrice(product.getPrice());
        dbProduct.setQuantity(product.getQuantity());
        dbProduct.setTitle(product.getTitle());
        return productReposiory.save(dbProduct);
    }

    public List<Product> save(List<Product> list){
        return productReposiory.saveAll(list);
    }

    public Product get(Long id) {
        return productReposiory.getById(id);
    }

    public List<Product> list(List<Long> idList) {
        return productReposiory.findBySkuIn(idList);
    }


}
