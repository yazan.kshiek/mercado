package com.mercado.product_synchronizer.batch;

import com.mercado.product_synchronizer.entity.Product;
import com.mercado.product_synchronizer.service.ProductService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductItemWriter implements ItemWriter<Product> {

    @Autowired
    private ProductService productService;
    @Override
    public void write(List<? extends Product> items) throws Exception {

        List<Long> idList = items.stream().map(Product::getSku).collect(Collectors.toList());
        List<Product> list = productService.list(idList);
        List<Product> toBeSaved = new ArrayList<>();
        items.forEach(i->{
            int index = list.indexOf(i);
            if(index > -1){
                Product db  = list.get(index);
                BeanUtils.copyProperties(i, db);
                toBeSaved.add(db);
            }else
                toBeSaved.add(i);
        });
        productService.save(toBeSaved);
        System.out.println("test");
    }
}
