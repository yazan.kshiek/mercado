package com.mercado.product_synchronizer.batch;


import com.mercado.product_synchronizer.entity.Product;
import org.springframework.batch.item.ItemProcessor;

public class ProductItemProcessor implements ItemProcessor<Product, Product> {

    @Override
    public Product process(Product product) throws Exception {
        return product;
    }

}