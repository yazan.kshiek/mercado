package com.mercado.product_synchronizer.repository;

import com.mercado.product_synchronizer.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findBySkuIn(List<Long> list);
}